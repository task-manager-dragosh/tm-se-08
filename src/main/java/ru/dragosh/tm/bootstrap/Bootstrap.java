package ru.dragosh.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.repository.ProjectRepositoryImplement;
import ru.dragosh.tm.repository.TaskRepositoryImplement;
import ru.dragosh.tm.repository.UserRepositoryImplement;
import ru.dragosh.tm.service.ProjectServiceImplement;
import ru.dragosh.tm.service.TaskServiceImplement;
import ru.dragosh.tm.service.UserServiceImplement;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class Bootstrap implements ServiceLocator {
    @Getter
    private ProjectService projectService = new ProjectServiceImplement(new ProjectRepositoryImplement());
    @Getter
    private TaskService taskService = new TaskServiceImplement(new TaskRepositoryImplement());
    @Getter
    private UserService userService = new UserServiceImplement(new UserRepositoryImplement());
    @Getter
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @Getter
    @Setter
    private User currentUser = new User("","", RoleType.INCOGNITO);

    public void init(@NotNull final Class[] COMMAND_CLASSES) {
        prepareCommands(COMMAND_CLASSES);
        prepareUsers();
        start();
    }

    private void start() {
        ConsoleUtil.log(MessageType.WELCOME_MESSAGE);
        ConsoleUtil.log(MessageType.HELP_MESSAGE);
        while (true) {
            String stringCommand = ConsoleUtil.readCommand().toLowerCase();
            AbstractCommand command = commands.get(stringCommand);

            if (command == null) {
                ConsoleUtil.log(MessageType.WRONG_COMMAND);
                continue;
            }
            if (!hasAccess(command)) {
                ConsoleUtil.log(MessageType.PERMISSION_DENIED);
                continue;
            }
            command.execute();
        }
    }

    private void prepareCommands(@NotNull Class[] COMMAND_CLASSES) {
        Predicate<Class> commandClassPredicate = (commandClass) -> {
            try {
                return (Class.forName(commandClass.getCanonicalName()).newInstance()) instanceof AbstractCommand;
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return false;
        };
        Stream
                .of(COMMAND_CLASSES)
                .filter(commandClassPredicate)
                .forEach(commandClass -> {
                    try {
                        AbstractCommand abstractCommand = (AbstractCommand) commandClass.newInstance();
                        abstractCommand.setServiceLocator(this);
                        commands.put(abstractCommand.getName(), abstractCommand);
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
    }

    private boolean hasAccess(final AbstractCommand command) {
        return command.getRoles().contains(currentUser.getRole());
    }

    private void prepareUsers() {
        userService.persist(new User("root", "root", RoleType.ADMIN));
        userService.persist(new User("user", "user", RoleType.USER));
    }
}
