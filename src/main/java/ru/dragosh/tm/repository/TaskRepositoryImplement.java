package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.TaskRepository;
import ru.dragosh.tm.entity.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepositoryImplement extends AbstractRepository<Task> implements TaskRepository {
    @Override
    public List<Task> findAll(@NotNull final String projectId) {
        return base.values().stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public Task find(@NotNull final String projectId, @NotNull final String taskName) {
        return base.values().stream()
                .filter(task -> task.getProjectId().equals(projectId) && task.getName().equals(taskName))
                .findFirst().orElse(null);
    }

    @Override
    public void removeAll(@NotNull final String projectId) {
        base.values().stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .forEach(task -> base.remove(task.getId()));
    }
}
