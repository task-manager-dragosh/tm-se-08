package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.ProjectRepository;
import ru.dragosh.tm.entity.Project;

import java.util.List;
import java.util.stream.Collectors;

public final class ProjectRepositoryImplement extends AbstractRepository<Project> implements ProjectRepository {
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        return base.values().stream()
                .filter(project -> project.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public Project find(@NotNull final String projectName, @NotNull final String userId) {
        return base.values().stream()
                .filter(project -> project.getName().equals(projectName) && project.getUserId().equals(userId)).
                        findFirst().orElse(null);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        base.values().stream()
                .filter(project -> project.getUserId().equals(userId))
                .forEach(project -> base.remove(project.getId()));
    }
}

