package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Entity;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<E extends Entity> implements Repository<E> {
    @NotNull protected final Map<String, E> base = new LinkedHashMap<>();

    @Override
    public void persist(final @NotNull E entity) {
        base.put(entity.getId(), entity);
    }

    @Override
    public void merge(final @NotNull E entity) {
        base.put(entity.getId(), entity);
    }

    @Override
    public void remove(final @NotNull String entityId) {
        base.remove(entityId);
    }
}
