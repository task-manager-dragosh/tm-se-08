package ru.dragosh.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.UUID;

public final class User {
    @Getter
    @Setter
    private String id = UUID.randomUUID().toString();
    @Getter
    @Setter
    private String login;
    @Getter
    @Setter
    private String password;
    @Getter
    @Setter
    private RoleType role;

    public User(String login, String password, RoleType role) {
        this.login = login;
        this.password = ConsoleUtil.getHash(password);
        this.role = role;
    }

    @Override
    public String toString() {
        return "UUID: " + this.id + ";\n" +
                "Login: " + this.login + ";\n" +
                "Password: " + this.password + ";\n" +
                "Role: " + this.role + ";\n";
    }
}
