package ru.dragosh.tm.entity;

public interface Entity {
    String getId();
    String getName();
    void setName(String name);
    String getDateStart();
    void setDateStart(String dateStart);
    String getDateFinish();
    void setDateFinish(String dateFinish);
    String getUserId();
    void setUserId(String userId);
}
