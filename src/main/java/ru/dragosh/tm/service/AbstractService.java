package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public abstract class AbstractService <E extends Entity, R extends Repository<E>>{
    protected R repository;
    protected AbstractService(final @NotNull R repository) {
        this.repository = repository;
    }

    public void persist(final @NotNull E entity) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        entity.setDateStart(dt.format(dt.parse(entity.getDateStart())));
        entity.setDateFinish(dt.format(dt.parse(entity.getDateFinish())));
        repository.persist(entity);
    }

    public void merge(final @NotNull E entity) {
        repository.merge(entity);
    }

    public void remove(final @NotNull String entityId) {
        repository.remove(entityId);
    }
}
