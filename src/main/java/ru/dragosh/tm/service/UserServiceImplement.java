package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.UserRepository;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.User;

public final class UserServiceImplement implements UserService {
    private final UserRepository userRepository;

    public UserServiceImplement(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User find(@NotNull final String login, @NotNull final String password) {
        return userRepository.find(login, password);
    }

    @Override
    public User findByLogin(@NotNull final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public void persist(@NotNull final User user) {
        userRepository.persist(user);
    }

    @Override
    public void merge(@NotNull final User user) {
        userRepository.merge(user);
    }

    @Override
    public void remove(@NotNull final String userId) {
        userRepository.remove(userId);
    }
}
