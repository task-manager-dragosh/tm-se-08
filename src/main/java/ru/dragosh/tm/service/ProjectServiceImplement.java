package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ProjectRepository;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.entity.Project;

import java.util.List;

public final class ProjectServiceImplement extends AbstractService<Project, ProjectRepository> implements ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectServiceImplement(@NotNull final ProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(@NotNull final String userId) {
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Project find(@NotNull final String projectName, @NotNull final String userId) {
        return projectRepository.find(projectName, userId);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        projectRepository.removeAll(userId);
    }
}
