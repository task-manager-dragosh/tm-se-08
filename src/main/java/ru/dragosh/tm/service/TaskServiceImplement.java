package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.TaskRepository;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.entity.Task;

import java.util.List;

public final class TaskServiceImplement extends AbstractService<Task, TaskRepository> implements TaskService {
    private final TaskRepository taskRepository;

    public TaskServiceImplement(@NotNull final TaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(@NotNull final String projectId) {
        return taskRepository.findAll(projectId);
    }

    @Override
    public Task find(@NotNull final String projectId, @NotNull final String nameTask) {
        return taskRepository.find(projectId, nameTask);
    }

    @Override
    public void removeAll(@NotNull final String projectId) {
        taskRepository.removeAll(projectId);
    }
}
