package ru.dragosh.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.enumeration.RoleType;

import java.util.Scanner;
import java.util.Set;

public abstract class AbstractCommand {
    @Getter
    @Setter
    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute();
    public abstract Set<RoleType> getRoles();

    public String readWord(@NotNull final String message) {
        System.out.print(message);
        @NotNull Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
}
