package ru.dragosh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

public class RegistrationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "register";
    }

    @Override
    public String getDescription() {
        return "(регистрация нового пользователя)";
    }

    @Override
    public void execute() {
        @Nullable String login = readWord("Введите ваш логин: ");
        @Nullable String password = readWord("Введите ваш пароль: ");
        if (login == null || login.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }

        if (password == null || password.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }

        if (serviceLocator.getUserService().findByLogin(login) != null) {
            ConsoleUtil.log(MessageType.USER_EXISTS);
            return;
        }

        @NotNull User user = new User(login, password, RoleType.USER);
        serviceLocator.getUserService().persist(user);
        serviceLocator.setCurrentUser(user);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.INCOGNITO);
        }};
    }
}
