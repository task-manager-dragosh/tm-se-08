package ru.dragosh.tm.command.user;


import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

public class AuthorisationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "authorise";
    }

    @Override
    public String getDescription() {
        return "(авторизация в программе)";
    }

    @Override
    public void execute() {
        @Nullable String login = readWord("Введите ваш логин: ");
        @Nullable String password = ConsoleUtil.getHash(readWord("Введите ваш пароль: "));
        if (login == null || login.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (password == null || password.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }

        @Nullable User user = serviceLocator.getUserService().find(login, password);
        if (user == null) {
            ConsoleUtil.log(MessageType.USER_NOT_FOUND);
            return;
        }
        serviceLocator.setCurrentUser(user);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.INCOGNITO);
        }};
    }
}
