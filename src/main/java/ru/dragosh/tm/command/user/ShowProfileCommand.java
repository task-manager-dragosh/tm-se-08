package ru.dragosh.tm.command.user;

import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.HashSet;
import java.util.Set;

public class ShowProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "show profile";
    }

    @Override
    public String getDescription() {
        return "(вывод информации о профиле пользователя)";
    }

    @Override
    public void execute() {
        ConsoleUtil.userOutput(serviceLocator.getCurrentUser());
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}
