package ru.dragosh.tm.command.app;

import com.jcabi.manifests.Manifests;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

public class AboutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "(вывод информации о сборке)";
    }

    @Override
    public void execute() {
        try {
            System.out.println("JAR was created by " + Manifests.read("Developer")
                    + " " + Manifests.read("AppVersion"));
        } catch (IllegalArgumentException ex) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
        }
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.INCOGNITO);
            add(RoleType.USER);
            add(RoleType.ADMIN);
        }};
    }
}
