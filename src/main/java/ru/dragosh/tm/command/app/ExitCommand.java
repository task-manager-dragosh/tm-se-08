package ru.dragosh.tm.command.app;

import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

public final class ExitCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "(после завершения ввода названия/названий введите \"exit\" + Enter)";
    }

    @Override
    public void execute() {
        ConsoleUtil.log(MessageType.PROGRAM_SHUTDOWN);
        System.exit(0);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.INCOGNITO);
            add(RoleType.USER);
            add(RoleType.ADMIN);
        }};
    }
}
