package ru.dragosh.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

public final class MergeProjectCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "change project name";
    }

    @Override
    public String getDescription() {
        return "(изменяет название определенного проекта в базе данных Projects)";
    }

    @Override
    public void execute() {
        @Nullable String projectName = readWord("Введите название проекта: ");
        @Nullable String newProjectName = readWord("Введите новое название для проекта: ");
        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        @Nullable Project project = serviceLocator.getProjectService().find(projectName, serviceLocator.getCurrentUser().getId());
        if (project == null) {
            ConsoleUtil.log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        if (serviceLocator.getProjectService().find(newProjectName, serviceLocator.getCurrentUser().getId()) == null) {
            ConsoleUtil.log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        project.setName(newProjectName);
        serviceLocator.getProjectService().merge(project);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}