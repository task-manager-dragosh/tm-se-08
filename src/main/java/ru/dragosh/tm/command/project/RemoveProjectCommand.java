package ru.dragosh.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

public final class RemoveProjectCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "remove project";
    }

    @Override
    public String getDescription() {
        return "(удаляет из Project base проект)";
    }

    @Override
    public void execute() {
        @Nullable ProjectService projectService = serviceLocator.getProjectService();
        @Nullable String projectName = readWord("Введите название проекта: ");
        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        @Nullable Project project = projectService.find(projectName, serviceLocator.getCurrentUser().getId());
        if (project == null) {
            ConsoleUtil.log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        projectService.remove(project.getId());
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}