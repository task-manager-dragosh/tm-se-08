package ru.dragosh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

public final class PersistProjectCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "add project";
    }

    @Override
    public String getDescription() {
        return "(добавляет в Project новый проект)";
    }

    @Override
    public void execute() {
        @NotNull ProjectService projectService = serviceLocator.getProjectService();
        @Nullable String projectName = readWord("Введите название проекта: ");
        @Nullable String projectDescription = readWord("Введите описание проекта: ");
        @Nullable String projectDateStart = readWord("Введите дату начала проекта: ");
        @Nullable String projectDateFinish = readWord("Введите дату окончания проекта: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (projectDescription == null || projectDescription.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (projectDateStart == null || projectDateStart.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (projectDateFinish == null || projectDateFinish.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (projectService.find(projectName, serviceLocator.getCurrentUser().getId()) != null) {
            ConsoleUtil.log(MessageType.PROJECT_EXISTS);
            return;
        }

        @Nullable Project project = new Project(projectName, projectDescription, projectDateStart, projectDateFinish, serviceLocator.getCurrentUser().getId());
        try {
            serviceLocator.getProjectService().persist(project);
        } catch (ParseException e) {
            ConsoleUtil.log(MessageType.WRONG_DATE_FORMAT);
        }
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}