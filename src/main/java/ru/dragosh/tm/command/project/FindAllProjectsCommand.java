package ru.dragosh.tm.command.project;

import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.HashSet;
import java.util.Set;

public final class FindAllProjectsCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "find all projects";
    }

    @Override
    public String getDescription() {
        return "(находит и выводит на экран все проекты из базы данных Projects)";
    }

    @Override
    public void execute() {
        serviceLocator.getProjectService().findAll(serviceLocator.getCurrentUser().getId()).forEach(ConsoleUtil::projectOutput);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}