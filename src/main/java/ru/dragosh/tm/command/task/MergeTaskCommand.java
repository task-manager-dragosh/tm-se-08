package ru.dragosh.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public final class MergeTaskCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "change task name";
    }

    @Override
    public String getDescription() {
        return "(изменяет название задачи из определенного проекта в базе данных Tasks)";
    }

    @Override
    public void execute() {
        @Nullable String projectName = readWord("Введите название проекта: ");
        @Nullable String taskName = readWord("Введите название задачи: ");
        @Nullable String newTaskName = readWord("Введите новое название для задачи: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (newTaskName == null || newTaskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        @Nullable Project project = serviceLocator.getProjectService().find(projectName, serviceLocator.getCurrentUser().getId());
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        @Nullable Task task = serviceLocator.getTaskService().find(project.getId(), taskName);
        if (task == null) {
            ConsoleUtil.log(MessageType.TASK_NOT_FOUND);
            return;
        }
        if (serviceLocator.getTaskService().find(project.getId(), newTaskName) != null) {
            ConsoleUtil.log(MessageType.TASK_EXISTS);
            return;
        }
        task.setName(newTaskName);
        serviceLocator.getTaskService().merge(task);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}