package ru.dragosh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

import static ru.dragosh.tm.util.ConsoleUtil.log;
import static ru.dragosh.tm.util.ConsoleUtil.taskOutput;

public final class FindTaskCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "find task";
    }

    @Override
    public String getDescription() {
        return "(находит и выводит на экран задачу по определенному проекту из базы данных Tasks)";
    }

    @Override
    public void execute() {
        @Nullable String projectName = readWord("Введите название проекта: ");
        @Nullable String taskName = readWord("Введите название задачи: ");

        if (projectName == null ||  projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        @NotNull Project project = serviceLocator.getProjectService().find(projectName, serviceLocator.getCurrentUser().getId());
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        @NotNull Task task = serviceLocator.getTaskService().find(project.getId(), taskName);
        if (task == null) {
            log(MessageType.TASK_NOT_FOUND);
            return;
        }
        taskOutput(task);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}