package ru.dragosh.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public final class PersistTaskCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "add task";
    }

    @Override
    public String getDescription() {
        return "(добавляет в проект новую задачу)";
    }

    @Override
    public void execute() {
        @Nullable String projectName = readWord("Введите название проекта: ");
        @Nullable String taskName = readWord("Введите название задачи: ");
        @Nullable String taskDescription = readWord("Введите описание задачи: ");
        @Nullable String taskDateStart = readWord("Введите дату начала выполнения задачи: ");
        @Nullable String taskDateFinish = readWord("Введите дату окончания выполнения задачи: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDescription == null || taskDescription.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDateStart == null || taskDateStart.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDateFinish == null || taskDateFinish.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }

        @Nullable Project project = serviceLocator.getProjectService().find(projectName, serviceLocator.getCurrentUser().getId());
        if (project == null) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (serviceLocator.getProjectService().find(project.getId(), taskName) != null) {
            ConsoleUtil.log(MessageType.TASK_EXISTS);
            return;
        }
        @Nullable Task task = new Task(taskName, taskDescription, taskDateStart, taskDateFinish, project.getId(), serviceLocator.getCurrentUser().getId());
        try {
            serviceLocator.getTaskService().persist(task);
        } catch (ParseException e) {
            log(MessageType.WRONG_DATE_FORMAT);
        }
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}