package ru.dragosh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public final class FindAllTasksCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "find all tasks";
    }

    @Override
    public String getDescription() {
        return "(находит и выводит на экран все задачи по определенному проекту из базы данных Tasks)";
    }

    @Override
    public void execute() {
        @Nullable String projectName = readWord("Введите название проекта: ");
        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        @Nullable Project project = serviceLocator.getProjectService().find(projectName, serviceLocator.getCurrentUser().getId());
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        @NotNull List<Task> taskList = serviceLocator.getTaskService().findAll(project.getId());
        taskList.forEach(ConsoleUtil::taskOutput);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}