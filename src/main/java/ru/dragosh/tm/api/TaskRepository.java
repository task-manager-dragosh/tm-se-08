package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Task;

import java.util.List;

public interface TaskRepository extends Repository<Task> {
    List<Task> findAll(String projectId);
    Task find(String projectId, String nameTask);
    void removeAll(String projectId);
}