package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Entity;

import java.text.ParseException;

public interface Repository<E extends Entity> {
    void persist(E entity) throws ParseException;
    void merge(E entity);
    void remove(String entityId);
}
