package ru.dragosh.tm;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.app.AboutCommand;
import ru.dragosh.tm.command.app.ExitCommand;
import ru.dragosh.tm.command.app.HelpCommand;
import ru.dragosh.tm.command.project.*;
import ru.dragosh.tm.command.task.*;
import ru.dragosh.tm.command.user.*;

public final class Application {
    private final static Class[] COMMAND_CLASSES = {
            FindAllProjectsCommand.class, FindProjectCommand.class,
            PersistProjectCommand.class, MergeProjectCommand.class,
            RemoveAllProjectsCommand.class, RemoveProjectCommand.class,

            FindAllTasksCommand.class, FindTaskCommand.class,
            PersistTaskCommand.class, MergeTaskCommand.class,
            RemoveAllTasksCommand.class, RemoveTaskCommand.class,

            AuthorisationCommand.class, LogoutCommand.class,
            RegistrationCommand.class, ShowProfileCommand.class,
            UpdateProfileCommand.class,

            AboutCommand.class, ExitCommand.class, HelpCommand.class
    };
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(COMMAND_CLASSES);
    }
}
